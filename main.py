from haarFaceDetector import cv2, haarFaceDetector
from CNNfaceRecognition import CNNfaceRecognition, threading
import time


class Choices():
    TRAIN_MODEL = 0
    ADD_FACE = 1
    RECOGNIZE_FACES = 2


def main():
    name_to_add = "roi"
    choice = Choices.RECOGNIZE_FACES
    faceDetector = haarFaceDetector()
    faceRcognition = CNNfaceRecognition(epochs=20)
    detectionsLock = threading.Lock()
    getNamesThread = None

    if choice == Choices.TRAIN_MODEL:
        faceRcognition.train()
    else:
        try:
            while cv2.waitKey(1) & 0xFF != ord("q"):
                detections = {}
                faces, videoFrame = faceDetector.detectFaces()
                if not len(faces): continue
                if choice == Choices.ADD_FACE: #loads only the first face
                    time.sleep(0.3)  # capture every n's a sec
                    mainFace = faces[0]
                    faceRcognition.addData(mainFace, videoFrame, name_to_add)
                    faceDetector.drawBoundingBox(videoFrame, mainFace, "Saving...")
                elif choice == Choices.RECOGNIZE_FACES:
                    if not(getNamesThread and getNamesThread.is_alive()):
                        getNamesThread = threading.Thread(target=faceRcognition.recognizeFaces, args=(videoFrame, faces, detections, detectionsLock))
                        getNamesThread.run()
                        faceRcognition.recognizeFaces(videoFrame, faces, detections, detectionsLock)
                    with detectionsLock:
                        for name, face in detections.items():
                            print(name, face)
                            faceDetector.drawBoundingBox(videoFrame, face, name)
                cv2.imshow("Smile! :)", videoFrame)

            faceDetector.video_capture.release()
            cv2.destroyAllWindows()
        except Exception as e:
            print(e)

if __name__ == '__main__':
    main()
