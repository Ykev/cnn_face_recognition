from keras.preprocessing.image import ImageDataGenerator
import tensorflow as tf
import threading
import pickle
import numpy
import cv2
import os

FACE_HEIGHT = 3
INFO_FILE_PATH = "info.txt"
MODEL_PATH = "CNN_model.keras"
CLASS_INDICES_PATH = "class_indices.pkl"


class CNNfaceRecognition:
    def __init__(self, data_path: str = "faces", epochs: int = 30,
                 batch_size: int = 3, image_size: tuple = (100, 100), num_classes: int = 1):
        self.data_path = data_path
        self.train_path = (data_path + "/Train")
        self.test_path = (data_path + "/Test")
        self.image_size = image_size  # Size to resize images
        self.batch_size = batch_size
        self.epochs = epochs
        self.num_classes = num_classes
        self.model = None
        self.class_indices = None

    def testModel(self):
        test_dataset = ImageDataGenerator(rescale=1 / 255).flow_from_directory(self.test_path,
                                                                               target_size=self.image_size,
                                                                               batch_size=self.batch_size,
                                                                               class_mode='categorical')
        print("testing...")
        test_loss, test_accuracy = self.model.evaluate(test_dataset)
        print("Test accuracy: ", test_accuracy * 100, "%")

    def loadModel(self):
        if (os.path.isfile(MODEL_PATH)):
            self.model = tf.keras.models.load_model(MODEL_PATH)

            with open(CLASS_INDICES_PATH, 'rb') as file:
                self.class_indices = pickle.load(file)
            # self.model = keras.models.load_model(MODEL_PATH, compile=True)
            return True
        return False

    def recognizeFaces(self, frame, faces: list, detections: dict, detectionsLock: threading.Lock):
        if not (self.loadModel()):
            # model not exists
            raise Exception("Please Train model before trying to use it")

        for face in faces:
            faceX = face[0]
            faceY = face[1]
            faceWidth = face[2]
            faceHeight = face[3]
            cropped_frame = frame[faceY:(faceY + faceHeight), faceX:(faceX + faceWidth)]
            cropped_frame = cv2.resize(cropped_frame, self.image_size)

            # Convert the frame to an array and add an extra dimension
            frame_array = numpy.expand_dims(cropped_frame, axis=0)
            frame_array = frame_array / 255.0  # Normalize pixel values

            prediction = self.model.predict(frame_array)
            prediction_index = numpy.argmax(prediction)
            print("prediction: ", prediction)
            print(prediction[0][prediction_index] * 100, '%')
            # key by value dict
            name = list(self.class_indices.keys())[prediction_index]
            with detectionsLock:
                detections[name] = face

    def addData(self, face: list, frame, name="newGuy"):
        print(face, face[1], face[2], face[3])
        faceX = face[0]
        faceY = face[1]
        faceWidth = face[2]
        faceHeight = face[3]
        print("adding to data set")
        new = False
        train_path = self.train_path + '/' + name
        test_path = self.test_path + '/' + name
        infoFilePath = train_path + '/' + INFO_FILE_PATH
        info_f = None
        # totally new environment
        if not os.path.exists(self.data_path):
            os.mkdir(self.data_path)
            os.mkdir(self.train_path)
            os.mkdir(self.test_path)

        # new person
        if not os.path.exists(train_path):
            os.mkdir(train_path)
            os.mkdir(test_path)
            info_f = open(infoFilePath, 'w+')
            info_f.write(str(1))
            info_f.close()

        info_f = open(infoFilePath, 'r+')
        print(info_f.readlines())
        info_f.seek(0)
        frameNumber = int(info_f.readlines()[0])
        print(frameNumber)
        if (frameNumber <= 5):
            newFramePath = test_path + f"/frame{frameNumber}.jpg"
        else:
            newFramePath = train_path + f"/frame{frameNumber}.jpg"
        print(newFramePath + "---saved to data---")
        # (x, y), (x + w, y + h) img[start_row:end_row, start_col:end_col]
        cropped_frame = frame[faceY:(faceY + faceHeight), faceX:(faceX + faceWidth)]
        cv2.imwrite(newFramePath, cropped_frame)
        info_f.seek(0)
        info_f.write(str(frameNumber + 1))
        info_f.close()

    def train(self):

        dataGenerator = ImageDataGenerator(rescale=1 / 255, validation_split=0.2)
        train_dataset = dataGenerator.flow_from_directory(self.train_path,
                                                          target_size=self.image_size,
                                                          batch_size=self.batch_size,
                                                          class_mode='categorical',  # 'categorical' for multiclass
                                                          subset='training')

        validation_dataset = dataGenerator.flow_from_directory(self.train_path,
                                                               target_size=self.image_size,
                                                               batch_size=self.batch_size,
                                                               class_mode='categorical',
                                                               subset='validation')

        self.class_indices = train_dataset.class_indices
        print(self.class_indices)
        self.num_classes = len(self.class_indices)

        self.model = tf.keras.models.Sequential(
            [tf.keras.layers.Conv2D(16, (3, 3), activation='relu', input_shape=(self.image_size[0],self.image_size[1], 3)),
             tf.keras.layers.MaxPool2D(2, 2),
             #
             tf.keras.layers.Conv2D(32, (3, 3), activation='relu'),
             tf.keras.layers.MaxPool2D(2, 2),
             #
             tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
             tf.keras.layers.MaxPool2D(2, 2),
             ##
             tf.keras.layers.Flatten(),
             ##
             tf.keras.layers.Dense(512, activation='relu'),
             ##
             tf.keras.layers.Dense(self.num_classes, activation='softmax')  # sigmoid for binary, softmax else
             ])

        self.model.compile(loss=tf.keras.losses.categorical_crossentropy,
                           optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),  # can use: RMSprop
                           metrics=['accuracy'])

        early_stopping = tf.keras.callbacks.EarlyStopping(monitor='loss', min_delta=0, patience=0, mode='auto',
                                                          verbose=1)
        self.model.fit(train_dataset,
                       epochs=self.epochs,
                       validation_data=validation_dataset,
                       callbacks=[early_stopping])

        self.testModel()
        # saving model and class indices
        self.model.save(MODEL_PATH, overwrite=True)
        with open(CLASS_INDICES_PATH, 'wb') as file:
            pickle.dump(self.class_indices, file)
