This project is a face recognition program written in Python. I did this project in order to learn and become more familiar with Convolutional neural network (CNN).
This project has three main features:

1) Gathering data, where the program will save a person's name and face images as training data for the model.
2) Training the model, where the program will train the CNN model and save it.
3) Face recognition, where the model will predict the names of all the people shown in the camera.  
