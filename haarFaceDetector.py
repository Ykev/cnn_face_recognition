import cv2

BOX_THICKNESS = 2
BOX_COLOR = (0, 255, 0)


class haarFaceDetector:

    def __init__(self):
        self.face_classifier = cv2.CascadeClassifier(
            cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
        self.video_capture = cv2.VideoCapture(0)

    def drawBoundingBox(self, frame, face: list, name: str):
        faceX = face[0]
        faceY = face[1]
        faceWidth = face[2]
        faceHeight = face[3]
        cv2.rectangle(frame, (faceX, faceY), (faceX + faceWidth, faceY + faceHeight), BOX_COLOR, BOX_THICKNESS)
        # cv2.rectangle(image, start_point, end_point, color, thickness)
        cv2.putText(frame, name, color=BOX_COLOR, thickness=BOX_THICKNESS, org=(faceX,faceY), fontFace=cv2.FONT_ITALIC, fontScale=1)

    def detectFaces(self):
        result, video_frame = self.video_capture.read()
        if result is False:
            raise Exception("Somthing went wrong with video capture, make sure camera isn't i use: ", result)

        gray_image = cv2.cvtColor(video_frame, cv2.COLOR_BGR2GRAY)
        faces = self.face_classifier.detectMultiScale(gray_image, 1.1, 5, minSize=(40, 40))
        return faces, video_frame
